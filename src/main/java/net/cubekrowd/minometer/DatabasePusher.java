package net.cubekrowd.minometer;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.util.StringJoiner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

public class DatabasePusher {
    public final String databaseName;
    public final String tablePrefix;
    public final String mysqlUri;
    public final String serverName;
    public final long expirationTimeMillis;

    public final long initialDelayMillis = 0;
    public final long repeatDelayMillis = 10 * 1000;
    public final long purgeDelayMillis = 2 * 60 * 60 * 1000;

    // filled in later when we connected to the database
    public short serverId;

    // NOTE(traks): purge on start up
    public long lastPurgeTimeMillis = -purgeDelayMillis;

    public BukkitTask currentScheduledAsyncTask;

    public final Object updateLock = new Object();
    public volatile State currentState = State.NOT_STARTED;

    public enum State {
        NOT_STARTED,
        INIT,
        RUNNING_PROPERLY,
        FAILED,
        STOPPED,
    }

    public DatabasePusher(String databaseName, String tablePrefix, String mysqlUri, String serverName, long expirationTimeMillis) {
        this.databaseName = databaseName;
        this.tablePrefix = tablePrefix;
        this.mysqlUri = mysqlUri;
        this.serverName = serverName;
        this.expirationTimeMillis = expirationTimeMillis;
    }

    public Logger getLogger() {
        return MinometerPlugin.instance.getLogger();
    }

    public String table(String name) {
        return databaseName + "." + tablePrefix + name;
    }

    public void start() {
        if (currentState != State.NOT_STARTED) {
            return;
        }

        currentState = State.INIT;
        long delayTicks = initialDelayMillis / 50;
        currentScheduledAsyncTask = Bukkit.getScheduler().runTaskLaterAsynchronously(MinometerPlugin.instance, this::runAsyncPartAndReschedule, delayTicks);
    }

    public void stopAndFinaliseNow() {
        if (currentState == State.NOT_STARTED || currentState == State.FAILED) {
            return;
        }

        // NOTE(traks): wait for async job to finish and cancel it
        getLogger().info("Waiting for database tasks to finish...");
        synchronized (updateLock) {
            currentScheduledAsyncTask.cancel();
        }
        getLogger().info("All tasks have finished");

        if (currentState == State.INIT) {
            // NOTE(traks): nothing
        } else if (currentState == State.RUNNING_PROPERLY) {
            // NOTE(traks): don't think there's much of a point in sending the
            // final logs to the database. It covers very little time.
            // getLogger().info("Sending final logs to the database");
            // updateData(false);
            // getLogger().info("All data has been transferred");
        }

        currentState = State.STOPPED;
    }

    public void runAsyncPartAndReschedule() {
        synchronized (updateLock) {
            if (currentScheduledAsyncTask.isCancelled()) {
                return;
            }

            var startTimeMillis = System.nanoTime() / 1_000_000;
            runAsyncPart();

            if (currentState != State.FAILED) {
                var newRunTime = startTimeMillis + repeatDelayMillis;
                var now = System.nanoTime() / 1_000_000;
                var delayTicks = Math.max(newRunTime - now, 0) / 50;
                currentScheduledAsyncTask = Bukkit.getScheduler().runTaskLaterAsynchronously(MinometerPlugin.instance, this::runAsyncPartAndReschedule, delayTicks);
            }
        }
    }

    public void runAsyncPart() {
        if (currentState == State.INIT) {
            try {
                setupDatabase();
            } catch (SQLTimeoutException e) {
                // connection failure, try again
                getLogger().warning("Can't connect to database, trying again soon");
            } catch (SQLException e) {
                getLogger().log(Level.SEVERE, "Failed to set up database", e);
                currentState = State.FAILED;
            }

            getLogger().info("Ready to update data");
            currentState = State.RUNNING_PROPERLY;
        } else if (currentState == State.RUNNING_PROPERLY) {
            updateData(true);
        }
    }

    public String createTable(String tableName, String... spec) {
        var layout = new StringJoiner(", ");
        var options = new StringJoiner(", ");

        int i = 0;
        for (; i < spec.length; i++) {
            if (spec[i].contains("=")) {
                options.add(spec[i]);
            } else {
                layout.add(spec[i]);
            }
        }

        return String.format("create table if not exists %s(%s) %s", table(tableName), layout, options);
    }

    public void setupDatabase() throws SQLException {
        try (var con = DriverManager.getConnection("jdbc:" + mysqlUri);
             var statement = con.createStatement()) {
            statement.execute("create database if not exists " + databaseName);
            // @NOTE(traks) sadly unsigned types don't work with prepared
            // statements, because the driver compiles the statements instead
            // and it'll error out

            statement.execute(createTable(
                    "servers",
                    "id tinyint unsigned not null auto_increment",
                    "name varchar(32) not null",
                    "primary key (id)",
                    "unique key (name)"
            ));

            // @NOTE(traks) rows as compact as possible:
            // data = time | duration | server_id
            statement.execute(createTable(
                    "ticks",
                    "data bigint not null",
                    // @NOTE(traks multiple servers will generate 100 million+
                    // rows, which will really slow down Grafana queries. An
                    // index speeds those queries up by several orders of
                    // magnitude!
                    "index using btree (data)",
                    "engine=aria",
                    "row_format=fixed"
            ));

            statement.execute(createTable(
                    "player_count",
                    "time bigint not null",
                    "count tinyint unsigned not null",
                    "server tinyint unsigned not null",
                    "engine=aria",
                    "row_format=fixed"
            ));

            statement.execute(createTable(
                    "player_status",
                    "time bigint not null",
                    "username varchar(16) not null",
                    "status tinyint not null",
                    "server tinyint unsigned not null",
                    "engine=aria",
                    "row_format=fixed"
            ));

            statement.execute(createTable(
                    "memory_usage",
                    "time bigint not null",
                    "heap smallint not null",
                    "used_eden smallint not null",
                    "used_old_gen smallint not null",
                    "used_survivor smallint not null",
                    "non_heap smallint not null",
                    "anon_rss smallint not null",
                    "file_rss smallint not null",
                    "shared_rss smallint not null",
                    "swap_anon_rss smallint not null",
                    "server tinyint unsigned not null",
                    "engine=aria",
                    "row_format=fixed"
            ));

            // generate unique ID for the server name. Use IDs because they take
            // up less space than string names.
            var res = statement.executeQuery(String.format("select id from %s where name='%s'", table("servers"), serverName));
            if (!res.next()) {
                // insert if not yet present. 'Ignore' just as an extra safety
                // precaution. Troublesome thing on MariaDB and maybe other SQL
                // servers is 'not ignore'/'on-duplicate-key-update' increment the
                // auto increment column even if there is nothing to insert.
                // Hence why we take this roundabout approach
                statement.execute(String.format("insert ignore into %s (name) values ('%s')", table("servers"), serverName));
                res = statement.executeQuery(String.format("select id from %s where name='%s'", table("servers"), serverName));
                res.next();
            }
            serverId = res.getShort("id");

            getLogger().info("Server ID is " + serverId);
        }
    }

    public void updateData(boolean allowPurge) {
        var newMeasurements = MinometerPlugin.measurements.grabAll();

        try (var con = DriverManager.getConnection("jdbc:" + mysqlUri)) {
            try (var statement = con.createStatement()) {
                for (var meas : newMeasurements) {
                    if (meas.hasTickDuration) {
                        var duration = Math.min(meas.endMillis - meas.beginMillis, 255);
                        statement.execute(String.format("insert into %s values (%d)",
                                table("ticks"), (meas.beginMillis << 16) | (duration << 8) | serverId));
                    }
                    if (meas.hasMemory) {
                        statement.execute(String.format(
                                "insert into %s values (%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)",
                                table("memory_usage"),
                                meas.timeMillis,
                                Math.min(meas.heapMbs, Short.MAX_VALUE),
                                Math.min(meas.usedEdenMbs, Short.MAX_VALUE),
                                Math.min(meas.usedOldGenMbs, Short.MAX_VALUE),
                                Math.min(meas.usedSurvivorMbs, Short.MAX_VALUE),
                                Math.min(meas.nonHeapMbs, Short.MAX_VALUE),
                                Math.min(meas.privateAnonymousRssMbs, Short.MAX_VALUE),
                                Math.min(meas.sharedFileRssMbs, Short.MAX_VALUE),
                                Math.min(meas.sharedAnonymousRssMbs, Short.MAX_VALUE),
                                Math.min(meas.privateAnonymousSwapMbs, Short.MAX_VALUE),
                                serverId
                        ));
                    }
                    if (meas.hasPlayerStatus) {
                        var status = meas.status.ordinal();
                        statement.execute(String.format("insert into %s values (%d, '%s', %d, %d)",
                                table("player_status"), meas.timeMillis, meas.playerName,
                                status, serverId));
                    }
                    if (meas.hasPlayerCount) {
                        var count = Math.min(meas.playerCount, 255);
                        statement.execute(String.format("insert into %s values (%d, %d, %d)",
                                table("player_count"), meas.timeMillis, count, serverId));
                    }
                }
            }

            // every so often we purge old data
            var now = System.nanoTime() / 1_000_000;
            if (allowPurge && now - lastPurgeTimeMillis >= purgeDelayMillis) {
                getLogger().info("Purging expired data from the database...");

                long purgeEndMillis = now - expirationTimeMillis;

                try (var purgeStatement = con.createStatement()) {
                    purgeStatement.execute(String.format("delete from %s where time < %d and server = %d", table("memory_usage"), purgeEndMillis, serverId));
                    // @NOTE(traks) need to delete data using '<' so the index
                    // is used. This really speeds things up on ticks tables
                    // with 100 million+ rows
                    purgeStatement.execute(String.format("delete from %s where data < %d and (data & 255) = %d", table("ticks"), purgeEndMillis << 16, serverId));
                    purgeStatement.execute(String.format("delete from %s where time < %d and server = %d", table("player_count"), purgeEndMillis, serverId));
                    purgeStatement.execute(String.format("delete from %s where time < %d and server = %d", table("player_status"), purgeEndMillis, serverId));
                    lastPurgeTimeMillis = now;
                }

                getLogger().info("Purge completed");
            }
        } catch (SQLTimeoutException e) {
            getLogger().warning("Can't connect to database");
        } catch (SQLException e) {
            getLogger().log(Level.SEVERE, "Something went wrong while updating data", e);
        }
    }
}

package net.cubekrowd.minometer;

import com.sun.management.GarbageCollectionNotificationInfo;
import java.lang.management.ManagementFactory;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;

public class GCNotificationListener implements NotificationListener {
    @Override
    public void handleNotification(Notification notification, Object handback) {
        if (!notification.getType().equals(GarbageCollectionNotificationInfo.GARBAGE_COLLECTION_NOTIFICATION)) {
            return;
        }

        var jvmStartMillis = ManagementFactory.getRuntimeMXBean().getStartTime();

        var info = GarbageCollectionNotificationInfo.from((CompositeData) notification.getUserData());
        var gcStartMillis = jvmStartMillis + info.getGcInfo().getStartTime();

        var plugin = (MinometerPlugin) handback;

        // @TODO(traks) Currently we do nothing! Perhaps we should dump the GC
        // logs into a table. Not sure how useful that would be, so let's first
        // see if basic memory information is interesting enough to make GC logs
        // desirable.

        // plugin.getLogger().info(String.format("[GC] %s %s: %s", info.getGcName(), info.getGcAction(), info.getGcCause()));
    }
}

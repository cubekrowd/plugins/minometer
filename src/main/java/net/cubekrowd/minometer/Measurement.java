package net.cubekrowd.minometer;

public class Measurement {
    public long timeMillis;
    public long beginMillis;
    public long endMillis;

    // tick
    public boolean hasTickDuration;

    // memory
    public boolean hasMemory;

    public int heapMbs;
    public int usedEdenMbs;
    public int usedOldGenMbs;
    public int usedSurvivorMbs;
    public int nonHeapMbs;

    public int privateAnonymousRssMbs;
    public int sharedFileRssMbs;
    public int sharedAnonymousRssMbs;
    public int privateAnonymousSwapMbs;

    // player status
    public boolean hasPlayerStatus;
    public PlayerStatus status;
    public String playerName;

    public enum PlayerStatus {
        LEAVE,
        JOIN
    }

    // player count
    public boolean hasPlayerCount;
    public int playerCount;
}

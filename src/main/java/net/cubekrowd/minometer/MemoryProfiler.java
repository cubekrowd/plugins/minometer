package net.cubekrowd.minometer;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryType;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Locale;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class MemoryProfiler implements Listener {
    public static int ceilDiv(long x, int y) {
        return (int) ((x + y - 1) / y);
    }

    @EventHandler
    public void onTickEnd(ServerTickEndEvent e) {
        if (Bukkit.getCurrentTick() % 40 == 0) {
            // Run async for the RSS fetching
            Bukkit.getScheduler().runTaskAsynchronously(MinometerPlugin.instance, () -> {
                // gather memory data
                var meas = new Measurement();
                meas.hasMemory = true;
                meas.timeMillis = Instant.now().toEpochMilli();

                var memoryBean = ManagementFactory.getMemoryMXBean();
                var heap = memoryBean.getHeapMemoryUsage();
                var nonHeap = memoryBean.getNonHeapMemoryUsage();

                var oneMb = 1024 * 1024;
                meas.heapMbs = ceilDiv(heap.getCommitted(), oneMb);
                meas.nonHeapMbs = ceilDiv(nonHeap.getCommitted(), oneMb);

                for (var poolBean : ManagementFactory.getMemoryPoolMXBeans()) {
                    var usage = poolBean.getUsage();
                    if (usage == null) {
                        continue;
                    }

                    var used = ceilDiv(usage.getUsed(), oneMb);

                    if (poolBean.getType() == MemoryType.HEAP) {
                        if (poolBean.getName().contains("Eden Space")) {
                            meas.usedEdenMbs += used;
                        } else if (poolBean.getName().contains("Old Gen")) {
                            meas.usedOldGenMbs += used;
                        } else if (poolBean.getName().contains("Survivor")) {
                            meas.usedSurvivorMbs += used;
                        }
                    }
                }

                if (System.getProperty("os.name").toLowerCase(Locale.ENGLISH).contains("linux")) {
                    pollProcStatus(meas);
                }

                MinometerPlugin.measurements.push(meas);
            });
        }
    }

    public void pollProcStatus(Measurement meas) {
        var pid = ProcessHandle.current().pid();
        try {
            var lines = Files.readAllLines(Path.of("/proc", String.valueOf(pid), "status"));

            for (var line : lines) {
                if (line.startsWith("RssAnon:")) {
                    meas.privateAnonymousRssMbs = (Integer.parseInt(line.strip().split("\s+")[1]) + 1024 - 1) / 1024;
                } else if (line.startsWith("RssFile:")) {
                    // @NOTE(traks) File = shared file, will get written back to
                    // disk if you write to the page. If file is mapped into
                    // memory with private set, then it'll be mapped into
                    // anonymous memory and you can't write it back to disk.
                    meas.sharedFileRssMbs = (Integer.parseInt(line.strip().split("\s+")[1]) + 1024 - 1) / 1024;
                } else if (line.startsWith("RssShmem:")) {
                    // @NOTE(traks) remaining shared memory
                    meas.sharedAnonymousRssMbs = (Integer.parseInt(line.strip().split("\s+")[1]) + 1024 - 1) / 1024;
                } else if (line.startsWith("VmSwap:")) {
                    // @NOTE(traks) doesn't include shared memory in swap
                    meas.sharedAnonymousRssMbs = (Integer.parseInt(line.strip().split("\s+")[1]) + 1024 - 1) / 1024;
                }
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
    }
}

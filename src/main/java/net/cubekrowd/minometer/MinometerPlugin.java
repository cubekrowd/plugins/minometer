package net.cubekrowd.minometer;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.util.HashSet;
import java.util.Set;
import javax.management.ListenerNotFoundException;
import javax.management.NotificationEmitter;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public final class MinometerPlugin extends JavaPlugin implements Listener {
    // Listeners push measurements into this list
    public static PushList<Measurement> measurements = new PushList<>();
    public static MinometerPlugin instance;

    public Set<GarbageCollectorMXBean> registeredGcBeans = new HashSet<>();
    public GCNotificationListener gcNotificationListener = new GCNotificationListener();
    public DatabasePusher databasePusher;

    @Override
    public void onEnable() {
        instance = this;

        saveDefaultConfig();
        var config = getConfig();
        var mysqlUri = config.getString("mysql.uri");
        var databaseName = config.getString("mysql.database");
        var tablePrefix = config.getString("mysql.table-prefix");
        var serverName = config.getString("server");
        if (serverName.length() > 32) {
            serverName = serverName.substring(0, 32);
        }
        var expirationTimeMillis = config.getInt("expiration-time") * 24 * 60 * 60 * 1000;
        var usingDatabase = config.getBoolean("auto-logging");

        databasePusher = new DatabasePusher(databaseName, tablePrefix, mysqlUri, serverName, expirationTimeMillis);

        if (serverName.isEmpty()) {
            getLogger().severe("The server name in the config must be set");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        getLogger().info("Server name is " + serverName);

        if (!usingDatabase) {
            // don't start logging. Don't need to listen for events either.
            getLogger().info("Not logging anything");
            return;
        }

        getLogger().info("Logging enabled");

        Bukkit.getPluginManager().registerEvents(new MemoryProfiler(), this);
        Bukkit.getPluginManager().registerEvents(new PlayerCountProfiler(), this);
        Bukkit.getPluginManager().registerEvents(new TickProfiler(), this);

        registerGcBeans();

        databasePusher.start();
    }

    @Override
    public void onDisable() {
        clearGcBeans();

        databasePusher.stopAndFinaliseNow();

        instance = null;
    }

    public void clearGcBeans() {
        for (var gcBean : registeredGcBeans) {
            try {
                ((NotificationEmitter) gcBean).removeNotificationListener(gcNotificationListener);
            } catch (ListenerNotFoundException e) {
                // ignore
            }
        }
        registeredGcBeans.clear();
    }

    public void registerGcBeans() {
        for (var gcBean : ManagementFactory.getGarbageCollectorMXBeans()) {
            if (gcBean instanceof NotificationEmitter) {
                ((NotificationEmitter) gcBean).addNotificationListener(gcNotificationListener, null, this);
                registeredGcBeans.add(gcBean);
            }
        }
    }
}

package net.cubekrowd.minometer;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import java.time.Instant;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerCountProfiler implements Listener {
    @EventHandler
    public void onServerTickEnd(ServerTickEndEvent e) {
        if (Bukkit.getCurrentTick() % 40 == 0) {
            var meas = new Measurement();
            meas.hasPlayerCount = true;
            meas.timeMillis = Instant.now().toEpochMilli();
            meas.playerCount = Bukkit.getOnlinePlayers().size();
            MinometerPlugin.measurements.push(meas);
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        var meas = new Measurement();
        meas.hasPlayerStatus = true;
        meas.timeMillis = Instant.now().toEpochMilli();
        meas.status = Measurement.PlayerStatus.JOIN;
        meas.playerName = e.getPlayer().getName();

        meas.hasPlayerCount = true;
        meas.playerCount = Bukkit.getOnlinePlayers().size();

        MinometerPlugin.measurements.push(meas);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        var meas = new Measurement();
        meas.hasPlayerStatus = true;
        meas.timeMillis = Instant.now().toEpochMilli();
        meas.status = Measurement.PlayerStatus.LEAVE;
        meas.playerName = e.getPlayer().getName();

        meas.hasPlayerCount = true;
        meas.playerCount = Bukkit.getOnlinePlayers().size();

        MinometerPlugin.measurements.push(meas);
    }
}

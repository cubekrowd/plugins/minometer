package net.cubekrowd.minometer;

import java.util.ArrayList;
import java.util.List;

public class PushList<T> {
    public final Object lock = new Object();
    public List<T> backingList = new ArrayList<>();

    public void push(T t) {
        synchronized (lock) {
            backingList.add(t);
        }
    }

    public List<T> grabAll() {
        var newList = new ArrayList<T>();
        synchronized (lock) {
            var res = backingList;
            backingList = newList;
            return res;
        }
    }
}

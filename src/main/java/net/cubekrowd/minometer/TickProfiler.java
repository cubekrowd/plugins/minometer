package net.cubekrowd.minometer;

import com.destroystokyo.paper.event.server.ServerTickEndEvent;
import com.destroystokyo.paper.event.server.ServerTickStartEvent;
import java.time.Instant;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class TickProfiler implements Listener {
    public Measurement currentTick;

    @EventHandler
    public void onServerTickStart(ServerTickStartEvent e) {
        currentTick = new Measurement();
        currentTick.hasTickDuration = true;
        currentTick.beginMillis = Instant.now().toEpochMilli();
    }

    @EventHandler
    public void onServerTickEnd(ServerTickEndEvent e) {
        if (currentTick == null) {
            return;
        }

        currentTick.endMillis = Instant.now().toEpochMilli();

        MinometerPlugin.measurements.push(currentTick);
        currentTick = null;
    }
}
